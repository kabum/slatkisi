<?php
    namespace App\Validators;

    use App\Core\Validator;
    use App\Validators\StringValidator;
    

    class TextValidator extends StringValidator {
        public function matchPattern(string $value) {
            if(!\preg_match( '|^([0-9a-zA-ZšŠđĐčČćĆžŽ\?\,\.\'\! ]){3,}$|', $value)){
                return false;
            }
            return $this->isValid($value);
        }
    }