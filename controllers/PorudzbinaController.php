<?php
namespace App\Controllers;

use App\Models\PorudzbinaModel;
use App\Models\ProizvodKorpaController;
use App\Models\ProizvodKorpaModel;
use App\Models\KategorijaModel;
use App\Models\KorpaModel;
use App\Core\Controller;

class PorudzbinaController extends Controller {
    public function getAdd($korpa_id) { 
        // $por = new PorudzbinaModel($this->getDatabaseConnection());
    }

    public function postAdd($korpa_id) {
        
        $ime_korisnika = filter_input(INPUT_POST, 'ime_korisnika', FILTER_SANITIZE_STRING);
        $prezime_korisnika = filter_input(INPUT_POST, 'prezime_korisnika', FILTER_SANITIZE_STRING);
        $adresa = filter_input(INPUT_POST, 'adresa', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
        $komentar = filter_input(INPUT_POST, 'komentar', FILTER_SANITIZE_STRING);

        // $broj_porudzbine = filter_input(INPUT_POST, 'broj_porudzbine',      FILTER_SANITIZE_NUMBER_INT);
        // $korpa_id = filter_input(INPUT_POST, 'korpa_id',                    FILTER_SANITIZE_NUMBER_INT);
        
        $por = new PorudzbinaModel($this->getDatabaseConnection());
                
        $porudzbinaId = $por->add([
            'ime_korisnika' => $ime_korisnika,
            'prezime_korisnika' => $prezime_korisnika,
            'adresa' => $adresa,
            'email' => $email,
            'komentar' => $komentar,
            'broj_porudzbine' => rand(1, 99999),
            'korpa_id' =>$korpa_id,
        ]);

        if (!$porudzbinaId) {
            $this->set('message', 'Došlo je do greške prilikom kreiranja porudžbine.');
            return;
        }

        $km = new KorpaModel($this->getDatabaseConnection());

        $korpaId = $km->editById($korpa_id, [
            'is_active' => false
        ]);

        
        
        $this->set('message', 'Uspesno ste kreirali porudžbinu!');
        return;

    }

}