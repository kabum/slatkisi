<?php
namespace App\Controllers;

use App\Models\ProizvodKorpaModel;
use App\Models\ProizvodModel;
use App\Models\KorpaModel;
use App\Core\Controller;
use App\Validators\NumberValidator;

class ProizvodKorpaController extends Controller {
    // public function getAdd() {
    //     $pk= new ProizvodKorpaModel($this->getDatabaseConnection());
    //     $items = $pk->getAll();
    //     $this->set('korpa', $items);
    // }

    // public function postAdd() {
    //     $proizvodId = filter_input(INPUT_POST, 'proizvodId', FILTER_SANITIZE_NUMBER_INT);
    //     $korpaId = filter_input(INPUT_POST, 'korpaId', FILTER_SANITIZE_NUMBER_INT);

    //     $pk = new ProizvodKorpaModel($this->getDatabaseConnection());
    //     $proizvod_korpaId = $pk->add([
    //         'proizvodId' => $proizvodId,
    //         'korpaId' => $korpaId
    //     ]);

    //     if (!$proizvod_korpaId) {
    //         $this->set('message', 'Došlo je do greške prilikom dodavanja porudžbine.');
    //         return;
    //     }

    //     \ob_clean();
    //     header('Location: ' . BASE . 'porudzbina/' . $porudzbinaId);
    //     exit;
    // }

    public function addProizvodKorpa(){
        $proizvodId = filter_input(INPUT_POST, 'proizvod_id', FILTER_SANITIZE_NUMBER_INT);

        $validator = (new NumberValidator())->setInteger()->setSigned();

        if(!$validator->isValid($proizvodId)){
            $this->getSession()->put('message', 'Neispravan proizvod.');
            header('Location: ' . BASE );
            exit;
        }

        $km = new KorpaModel($this->getDatabaseConnection());

        $korpe = $km->getAll();
        $korpaId = null;

        if(!$korpe){
            $korpaId = $km->add([]);
        }
        else{
            foreach($korpe as $korpa){
                if($korpa->is_active){
                    $korpaId = $korpa->korpa_id;
                }
            }
        }

        if($korpaId == null){
            $korpaId = $km->add([]);
        }

        $pkm = new ProizvodKorpaModel($this->getDatabaseConnection());

        $proizvod_korpaId = $pkm->add([
            'korpa_id'      => $korpaId,
            'proizvod_id'   => $proizvodId
        ]);

        $this->getSession()->put('message', 'Uspešan unos.');
        header('Location: ' . BASE );
        exit;
    }

    public function getKorpa(){
        $km = new KorpaModel($this->getDatabaseConnection());

        $korpe = $km->getAll();
        $korpaId = null;
        
        if(!$korpe){
            $this->set('message', 'Korpa je prazna.');
            $this->set('korpa', false);
            return;
        }
        else{
            foreach($korpe as $korpa){
                if($korpa->is_active){
                    $korpaId = $korpa->korpa_id;
                }
            }
        }
        if($korpaId == null){
            $this->set('message', 'Korpa je prazna.');
            $this->set('korpa', false);
            return;
        }

        $pkm = new ProizvodKorpaModel($this->getDatabaseConnection());

        $proizvodiKorpa = $pkm->getAllByFieldName('korpa_id', $korpaId);

        if(!$proizvodiKorpa){
            $this->set('message', 'Korpa je prazna.');
            $this->set('korpa', false);
            return;
        }

        $pm = new ProizvodModel($this->getDatabaseConnection());

        $proizvodi = array();
        foreach($proizvodiKorpa as $proizvodKorpa){
            array_push($proizvodi, $pm->getById($proizvodKorpa->proizvod_id));
        }

        $this->set('message', $this->getSession()->get('message'));
        $this->getSession()->remove('message');

        $this->set('korpa', $korpaId);
        $this->set('proizvodi', $proizvodi);
    } 

    public function deleteProizvod(){
        $korpaId = filter_input(INPUT_POST, 'korpa_id', FILTER_SANITIZE_NUMBER_INT);
        $proizvodId = filter_input(INPUT_POST, 'proizvod_id', FILTER_SANITIZE_NUMBER_INT);

        $validator = (new NumberValidator())->setInteger()->setSigned();

        if(!$validator->isValid($korpaId)){
            $this->getSession()->put('message', 'Neispravna korpa.');
            header('Location: ' . BASE . 'korpa');
            exit;
        }

        if(!$validator->isValid($proizvodId)){
            $this->getSession()->put('message', 'Neispravan proizvod.');
            header('Location: ' . BASE . 'korpa');
            exit;
        }

        $pkm = new ProizvodKorpaModel($this->getDatabaseConnection());

        $pkId = $pkm->getByKorpaAndProizvod($korpaId, $proizvodId);

        if(!$pkId){
            $this->getSession()->put('message', 'Ne postoji unos za zadati uslov.');
            header('Location: ' . BASE . 'korpa');
            exit;
        }

        $deletedId = $pkm->deleteById($pkId);


        if(!$deletedId){
            $this->getSession()->put('message', 'Nije uspelo brisanje.');
            header('Location: ' . BASE . 'korpa');
            exit;
        }

        $this->getSession()->put('message', 'Uspešno brisanje.');
        
        header('Location: ' . BASE . 'korpa');
        exit;

    }
    
    }