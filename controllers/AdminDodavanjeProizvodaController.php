<?php
namespace App\Controllers;

use App\Core\AdminController;
use App\Models\ProizvodModel;
use App\Models\KorpaModel;
use App\Models\KategorijaModel;
use App\Validators\TextValidator;
use App\Validators\SlovaValidator;
use App\Validators\StringValidator;
use App\Validators\NumberValidator;
use App\Validators\JedMereValidator;

class AdminDodavanjeProizvodaController extends AdminController
{
    public function proizvodi()
    { //pribavljanje baze i tabele
        $pm = new ProizvodModel($this->getDatabaseConnection());
        $items = $pm->getAll();
        $this->set('proizvodi', $items);
    }
    public function getAdd()
    {
        //$pm= new ProizvodModel($this->getDatabaseConnection());
        $km = new KategorijaModel($this->getDatabaseConnection());
        $kom= new KorpaModel($this->getDatabaseConnection());
        //$proizvodi = $pm->getAll();
        $kategorije= $km->getAll();
        $korpe = $kom->getAll();
        //$this->set('proizvodi', $proizvodi);
        $this->set('kategorije', $kategorije);
        $this->set('korpe', $korpe);
        //  Izvuci iz baze sve kategorije i posalji na view
    }
  
    //za dodavanje slike
    private function doUpload($fieldName, $filename)
    {
        $path = new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
        $file = new \Upload\File($fieldName, $path);
        $file->setName($filename);
        $file->addValidations([
            new \Upload\Validation\Mimetype('image/jpeg'),
            new \Upload\Validation\Size('3M')
        ]);

        try {
            $file->upload();
            //return true;
        } catch (\Exception $e) {
            $this->set('message', 'Došlo je do greške: ' . implode(', ', $file->getErrors()));
            ///return false;
        }
    }


    public function postAdd()
    { //dodavanje proizvoda
        $naziv = filter_input(INPUT_POST, 'naziv', FILTER_SANITIZE_STRING);
        $opis = filter_input(INPUT_POST, 'opis', FILTER_SANITIZE_STRING);
        $sastojci = filter_input(INPUT_POST, 'sastojci', FILTER_SANITIZE_STRING);
        $cena = filter_input(INPUT_POST, 'cena', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $jed_mere = filter_input(INPUT_POST, 'jed_mere', FILTER_SANITIZE_STRING);
        $kategorija_id = filter_input(INPUT_POST, 'kategorija_id', FILTER_SANITIZE_NUMBER_INT);

        
        if(!$this->validateInputs($naziv, $opis,$sastojci,$cena,$jed_mere,$kategorija_id)){
            $this->set('message', 'Došlo je do greške u poljima za unos.');
            return;
        }


        $pm = new ProizvodModel($this->getDatabaseConnection());

        $proizvodId = $pm->add([
            'naziv' => $naziv,
            'opis' => $opis,
            'sastojci' => $sastojci,
            'cena' => $cena,
            'jed_mere' => $jed_mere,
            'kategorija_id' => $kategorija_id,
            'image_path' => rand(1000, 9999)
            //'admin_id' => $this->getSession()->get('adminId'),
            
        ]);

        if (!$proizvodId) {
            $this->set('message', 'Došlo je do greške prilikom dodavanja novog proizvoda.');
            return;
        }

        if (!$this->doUpload('image', $proizvodId)) {
            return;
        }

        \ob_clean();
        header('Location: ' . BASE . 'admin/proizvodi');
        exit;
    }

    

    public function getEdit($id)
    { //pribavljanje id za izmenu proizvoda
        $pm = new ProizvodModel($this->getDatabaseConnection());

        
        $proizvod = $pm->getById($id);
        
        if (!$proizvod) {
            \ob_clean();
            header('Location: ' . BASE . 'admin/home');
            exit;
        }

        $km = new KategorijaModel($this->getDatabaseConnection());

        $kategorije = $km->getAll();
        
        $this->set('proizvod', $proizvod);
        $this->set('kategorije', $kategorije);
    }
    
    
    public function postEdit($id)
    { //izmena proizvod i dodavanje
        $naziv = filter_input(INPUT_POST, 'naziv', FILTER_SANITIZE_STRING);
        $opis = filter_input(INPUT_POST, 'opis', FILTER_SANITIZE_STRING);
        $sastojci = filter_input(INPUT_POST, 'sastojci', FILTER_SANITIZE_STRING);
        $cena = filter_input(INPUT_POST, 'cena', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $jed_mere = filter_input(INPUT_POST, 'jed_mere', FILTER_SANITIZE_STRING);
        $kategorija_id = filter_input(INPUT_POST, 'kategorija_id', FILTER_SANITIZE_NUMBER_INT);
        
        if(!$this->validateInputs($naziv, $opis,$sastojci,$cena,$jed_mere,$kategorija_id,$id)){
           $this->set('message', 'Došlo je do greške u poljima za unos.');
           return;
        }

        $pm = new ProizvodModel($this->getDatabaseConnection());
        $res = $pm->editById($id, [
            'naziv' => $naziv,
            'opis' => $opis,
            'sastojci' => $sastojci,
            'cena' => $cena,
            'jed_mere' => $jed_mere,
            'kategorija_id' => $kategorija_id,
        ]);

        if (!$res) {
            $this->set('message', 'Došlo je do greške prilikom izmene ovog proizvoda.');
            return;
        }
        
        \ob_clean();
        header('Location: ' . BASE . 'admin/proizvodi');
        exit;
    }

    private function validateInputs($naziv, $opis,$sastojci,$cena,$jed_mere,$kategorija_id,$proizvodId = false)
    {
        $validator = (new TextValidator())->setMinLength(3)->setMaxLength(65);
        if (! $validator->matchPattern($naziv)) {
            $this->set('message', 'Naziv mora sadržati najmanje 3 vidljiva uzastopna karaktera.');
            if ($proizvodId) {
                $this->set('proizvod_id', $proizvodId);
            }
            return false;
        }

        $validator = (new TextValidator())->setMinLength(3)->setMaxLength(60000);
        if (! $validator->matchPattern($opis)) {
            $this->set('message', 'Opis mora sadržati najmanje 3 vidljivih uzastopna karaktera.');
            if ($proizvodId) {
                $this->set('proizvod_id', $proizvodId);
            }
            return false;
        }

        $validator = (new TextValidator())->setMinLength(3)->setMaxLength(6000);
        if (! $validator->matchPattern($sastojci)) {
            $this->set('message', 'Spisak sastojaka mora sadržati najmanje 3 sastojka.');
            if ($proizvodId) {
                $this->set('proizvod_id', $proizvodId);
            }
            return false;
        }
        $validator = (new NumberValidator());
        if (! $validator->isValid($cena)) {
            $this->set('message', 'Cena nije dobra.');
            if ($proizvodId) {
                $this->set('proizvod_id', $proizvodId);
            }
            return false;
        }

        $validator = (new JedMereValidator());
        if (! $validator->isValid($jed_mere)) {
            $this->set('message', 'Jedinica mere proizvoda mora biti "kg" ili "kom". -> ' . $jed_mere);
            if ($proizvodId) {
                $this->set('proizvod_id', $proizvodId);
            }
            return false;
        }

        $validator = (new NumberValidator())->setInteger();
        if (! $validator->isValid($kategorija_id)) {
            $this->set('message', 'Kategorija proizvoda nije dobra.');
            if ($proizvodId) {
                $this->set('proizvod_id', $proizvodId);
            }
            return false;
        }


        return true;
    }
}
