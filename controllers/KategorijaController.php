<?php
namespace App\Controllers;

use App\Models\KategorijaModel;
use App\Core\Controller;

class KategorijaController extends Controller {
    public function show($kategorijaId) {
        $km = new KategorijaModel($this->getDatabaseConnection());
        $kategorija = $km->getById($kategorijaId);

        if ( !$km->isActive($kategorija) ) {
            ob_clean();
            header('Location: ' . BASE);
            exit;
        }
    }

    
}
