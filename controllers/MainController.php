<?php
namespace App\Controllers;

use App\Models\ProizvodModel;
use App\Core\Controller;
use App\Models\KategorijaModel;
use App\Models\AdminModel;
use App\Validators\StringValidator;

class MainController extends Controller {
    public function home() {
        $pm = new ProizvodModel($this->getDatabaseConnection());
        $km = new KategorijaModel($this->getDatabaseConnection());
        $proizvodi = $pm->getAll();
        $kategorije = $km->getAll();

        $aktivniProizvodi = array();
        $aktivneKategorije = array();

        foreach($kategorije as $kategorija){
            if($kategorija->is_active == 1){
                array_push($aktivneKategorije, $kategorija);
            }
        }

        foreach($proizvodi as $proizvod){
            foreach($aktivneKategorije as $kategorija){
                if($proizvod->kategorija_id == $kategorija->kategorija_id){
                    array_push($aktivniProizvodi, $proizvod);
                }
            }
        }

        usort($aktivniProizvodi, function($a, $b) {
            return $b->cena <=> $a->cena;
        });
        
        $this->set('message', $this->getSession()->get('message'));
        $this->getSession()->remove('message');
        $this->set('kategorije', $aktivneKategorije);
        $this->set('proizvodi', $aktivniProizvodi);
    }

    public function proizvodiSortedById() {
        $pm = new ProizvodModel($this->getDatabaseConnection());
        $proizvodi = $pm->getAll();

        usort($proizvodi, function($a, $b) {
            return $b->naziv <=> $a->naziv;
        });

        

        $this->set('proizvodi', $proizvodi);
    }

    // public function showKategorijeProizvoda($proizvodId) {
    //     $km = new KategorijaModel($this->getDatabaseConnection());
    //     $kategorije = $km->getAllActiveByProizvodId($proivodId);
    //     $this->set('kategorije', $kategorije);

    //     $pm = new ProizvodModel($this->getDatabaseConnection());
    //     $proizvod = $pm->getById($proizvodId);
    //     $this->set('proizvod', $proizvod);
    // }

    public function showProizvod($proizvodId){
        $pm = new ProizvodModel($this->getDatabaseConnection());
        $proizvod = $pm->getById($proizvodId);

        $this->set('proizvod', $proizvod);
    }

    public function loginGet() {
        
    }
    public function loginOut() {
        $this->getSession()->remove('adminId');
        $this->getSession()->save();
        header('Location: ' . BASE );
        exit;
    }

    public function dajsesiju(){
        print_r($this->getSession()->getData());exit;
    }

    public function loginPost() {
        $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

        $am = new AdminModel($this->getDatabaseConnection());

        $admin = $am->getByFieldName('username', $username);

        if (!$admin) {
            sleep(1);
            $this->set('message', 'Loši podaci!');
            return;
        }


        if (!password_verify($password, $admin->password)) {
            sleep(1);
            $this->set('message', 'Loši podaci!');
            return;
        }

        $this->getSession()->put('adminId', $admin->admin_id);

        \ob_clean();
        header('Location: ' . BASE . 'admin/home/');
        exit;
    }
    
}
