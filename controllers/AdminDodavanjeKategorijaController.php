<?php
namespace App\Controllers;

use App\Core\AdminController;
use App\Models\ProizvodModel;
use App\Models\KorpaModel;
use App\Models\KategorijaModel;
use App\Models\PorudzbinaModel;
use App\Validators\BitValidator;
use App\Validators\TextValidator;

class AdminDodavanjeKategorijaController extends AdminController {

    public function kategorije(){
        $km = new KategorijaModel($this->getDatabaseConnection());
        $kategorije= $km->getAll();
        $this->set('kategorije', $kategorije);
    }

    public function getAdd() { //prikupljanje dodatih proizvoda da bi se napravila kategorija za svaki
        $pm= new ProizvodModel($this->getDatabaseConnection());
        $kom= new KorpaModel($this->getDatabaseConnection()); //Korpa
        $proizvodi = $pm->getAll();
        $korpe = $kom->getAll();
        $this->set('korpe', $korpe);
        $this->set('proizvodi', $proizvodi);
    }

   
               // dodavanje kategorije i njenih parametara
    public function postAdd() {
        $naziv = filter_input(INPUT_POST, 'naziv', FILTER_SANITIZE_STRING);

        $km = new KategorijaModel($this->getDatabaseConnection());

        $kategorijaId = $km->add([
            'naziv' => $naziv,
            'admin_id' => $this->getSession()->get('adminId'),
        ]);

        if (!$kategorijaId) {
            $this->set('message', 'Došlo je do greške prilikom dodavanja nove kategorije proizvoda.');
            return;
        }

        \ob_clean();
        header('Location: ' . BASE . 'admin/kategorije');
        exit;

    }
    public function getEdit($kategorija_id) { //pribavljanje id za izmenu kategorija
        $km = new KategorijaModel($this->getDatabaseConnection());

        $kategorija = $km->getById($kategorija_id);

        if (!$kategorija) {
            \ob_clean();
            header('Location: ' . BASE . 'admin/kategorije');
            exit;
        }

        $this->set('kategorija', $kategorija);
    }
    
    
    public function postEdit($kategorija_id) { //izmena kategorije i dodavanje
        $naziv = filter_input(INPUT_POST, 'naziv', FILTER_SANITIZE_STRING);
        $isActive = filter_input(INPUT_POST, 'is_active', FILTER_SANITIZE_NUMBER_INT);

        //print_r($this->getSession()->get('adminId'));exit;
        
        $validator = (new TextValidator())->setMinLength(3)->setMaxLength(50);

        if(!$validator->matchPattern($naziv)){
            // $this->getSession()->put('message', 'Neispravan .');
            $this->set('message', 'Neispravan naziv kategorije.');
            return;
        }

        $validator = new BitValidator();

        if(!$validator->isValid($isActive)){
            $this->set('message', 'Neispravna vrednost vidljivosti.');
            return;
        }
        
        if($isActive == 1){
            $isActive = true;
        }
        else{
            $isActive = false;
        }

        $km = new KategorijaModel($this->getDatabaseConnection());
      
        $res = $km->editById($kategorija_id,[
            'naziv' => $naziv,
            'is_active' => $isActive,
        ]);

        if (!$res) {
            $this->set('message', 'Došlo je do greške prilikom izmene ove kategorije.');
            return;
        }
        //print_r($res);exit;
        \ob_clean();
        header('Location: ' . BASE . 'admin/kategorije');
        exit;
    }
}
