<?php
namespace App\Controllers;

use App\Models\ProizvodModel;
use App\Core\Controller;

class ProizvodController extends Controller
{


    public function postSearch() {
        $keyword = filter_input(INPUT_POST, 'search', FILTER_SANITIZE_STRING);

        $pm = new ProizvodModel($this->getDatabaseConnection());

        $proizvodi = $pm->getAllBySearch($keyword);

        $this->set('proizvodi', $proizvodi);
    }
}