<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;
    use \PDO;

    class ProizvodKorpaModel extends Model {
        protected function getFields() {
            return [
                'proizvod_korpa_id' => new Field(
                                        (new NumberValidator())
                                            ->setInteger()
                                            ->setUnsigned()
                                            ->setMaxIntegerDigits(10), false),
                'proizvod_id'             => new Field(
                                        (new NumberValidator())
                                            ->setInteger()
                                            ->setUnsigned()
                                            ->setMaxIntegerDigits(10)),
                'korpa_id'       => new Field(
                                        (new NumberValidator())
                                            ->setInteger()
                                            ->setUnsigned()
                                            ->setMaxIntegerDigits(10) )
            ];
        }

        public function getByKorpaAndProizvod($korpaId, $proizvodId) {
            
            $pdo = $this->getDatabaseConnection()->getConnection();
            $sql = 'SELECT * FROM proizvod_korpa WHERE korpa_id = ? AND proizvod_id = ?;';
            $prep = $pdo->prepare($sql);
            $item = null;

            if ($prep) {
                $prep->execute( [ $korpaId, $proizvodId ] );

                $item = $prep->fetch(PDO::FETCH_OBJ);

                if (!$item) {
                    $item = null;
                }
            }

            return $item->proizvod_korpa_id;
        }
    }
