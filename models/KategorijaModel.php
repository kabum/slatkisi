<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;
    use \PDO;

    class KategorijaModel extends Model {
        protected function getFields() {
            return [
                'kategorija_id'     => new Field(
                                        (new NumberValidator())
                                            ->setInteger()
                                            ->setUnsigned()
                                            ->setMaxIntegerDigits(10), false),
                'naziv'             => new Field(
                                        (new StringValidator())
                                            ->setMinLength(1)
                                            ->setMaxLength(255)),
                    
                'admin_id'           => new Field(
                                        (new NumberValidator())
                                             ->setInteger()
                                             ->setUnsigned()
                                             ->setMaxIntegerDigits(10)),
                'is_active'        => new Field(
                                     new BitValidator()),
            ];
        }
 }
