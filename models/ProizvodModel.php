<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\StringValidator;
    use App\Validators\NumberValidator;
    use App\Validators\BitValidator;
    use \PDO;
   

    class ProizvodModel extends Model {
        protected function getFields() {
            return [
                'proizvod_id'        => new Field(
                                    (new NumberValidator())
                                        ->setInteger()
                                        ->setUnsigned()
                                        ->setMaxIntegerDigits(10), false),
                'naziv'        => new Field(
                                    (new StringValidator())
                                        ->setMinLength(1)
                                        ->setMaxLength(255)),
                'opis'         => new Field(
                                    (new StringValidator())
                                        ->setMinLength(1)
                                        ->setMaxLength(255)),
                'sastojci'         => new Field(
                                    (new StringValidator())
                                        ->setMinLength(1)
                                        ->setMaxLength(255)),
                'cena'         => new Field( 
                                    (new NumberValidator())), 
                'jed_mere'     => new Field(
                                    (new StringValidator())
                                        ->setMinLength(2)
                                        ->setMaxLength(3)), 
                'kategorija_id'  => new Field(
                                    (new NumberValidator())
                                        ->setInteger()
                                        ->setUnsigned()
                                        ->setMaxIntegerDigits(10)), 
                'image_path'  => new Field(
                                            (new StringValidator())
                                            ->setMinLength(1)
                                            ->setMaxLength(255)),  
                    
                        
            ];
        }

        
    }
