<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\DateTimeValidator;
    use \PDO;

    class PorudzbinaModel extends Model {
        protected function getFields() {
            return [
                'porudzbina_id'    => new Field(
                                    (new NumberValidator())
                                        ->setInteger()
                                        ->setUnsigned()
                                        ->setMaxIntegerDigits(10), false),
                'ime_korisnika' => new Field(
                                    (new StringValidator())
                                        ->setMinLength(1)
                                        ->setMaxLength(64)),
                'prezime_korisnika' => new Field(
                                    (new StringValidator())
                                        ->setMinLength(1)
                                        ->setMaxLength(64)),
                'adresa' => new Field(
                                     (new StringValidator())
                                        ->setMinLength(1)
                                        ->setMaxLength(64)),
                'email' => new Field(
                                    (new StringValidator())
                                        ->setMinLength(1)
                                        ->setMaxLength(64)),
                'komentar' => new Field(
                                    (new StringValidator())
                                         ->setMinLength(1)
                                         ->setMaxLength(255)),                        
                                        
                'broj_porudzbine'     => new Field(
                                    (new NumberValidator())
                                        ->setInteger()
                                        ->setUnsigned()
                                        ->setMaxIntegerDigits(10)),
                
                'korpa_id'    => new Field(
                                    (new NumberValidator())
                                        ->setInteger()
                                        ->setUnsigned()
                                        ->setMaxIntegerDigits(10)),
                ];
        }

