-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2020 at 08:34 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `slatkisi`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `username`, `password`) VALUES
(1, 'Ivana', '$2y$10$WqhpCyOgDBx08wPVexgp5envegPBHniaEyzSNqZAK5TR4WYyuYCLa');

-- --------------------------------------------------------

--
-- Table structure for table `kategorija`
--

CREATE TABLE `kategorija` (
  `kategorija_id` int(10) UNSIGNED NOT NULL,
  `naziv` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `is_active` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `kategorija`
--

INSERT INTO `kategorija` (`kategorija_id`, `naziv`, `admin_id`, `is_active`) VALUES
(1, 'Sitni kolači', 1, b'0'),
(2, 'Torte', 1, b'1'),
(3, 'Kolači', 1, b'1'),
(4, 'Pite', 1, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `korpa`
--

CREATE TABLE `korpa` (
  `korpa_id` int(10) UNSIGNED NOT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `korpa`
--

INSERT INTO `korpa` (`korpa_id`, `is_active`) VALUES
(1, b'0'),
(2, b'0'),
(3, b'0'),
(4, b'0'),
(5, b'0'),
(6, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `porudzbina`
--

CREATE TABLE `porudzbina` (
  `porudzbina_id` int(10) UNSIGNED NOT NULL,
  `ime_korisnika` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `prezime_korisnika` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `adresa` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `komentar` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `broj_porudzbine` int(10) UNSIGNED NOT NULL,
  `korpa_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `porudzbina`
--

INSERT INTO `porudzbina` (`porudzbina_id`, `ime_korisnika`, `prezime_korisnika`, `adresa`, `email`, `komentar`, `broj_porudzbine`, `korpa_id`) VALUES
(1, 'Pera', 'Peric', 'Eheheh', 'pera@peric.rs', 'Najbolji kolaci ikada', 65858, 3),
(2, 'Ivana', 'Tomic', 'Arandjelovac', 'ana123@yahoo.com', 'Jedva cekam 100 evra od Vucica', 1915, 6);

-- --------------------------------------------------------

--
-- Table structure for table `proizvod`
--

CREATE TABLE `proizvod` (
  `proizvod_id` int(10) UNSIGNED NOT NULL,
  `naziv` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `opis` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sastojci` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cena` double(4,0) UNSIGNED NOT NULL,
  `jed_mere` enum('kg','kom') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `kategorija_id` int(10) UNSIGNED NOT NULL,
  `image_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `proizvod`
--

INSERT INTO `proizvod` (`proizvod_id`, `naziv`, `opis`, `sastojci`, `cena`, `jed_mere`, `kategorija_id`, `image_path`) VALUES
(2, 'Kolacici', 'Slatki kolacici', '', 50, 'kom', 3, '9992'),
(3, 'Krempita', '                  Dekorativne hrskave I medne korice, su izuzetno ukusne, i pucaju pri svakom zalogaju.', '                  puding,mleko,kora,šećer', 110, 'kom', 3, '7529'),
(4, 'Pita sa visnjom', '                  Ukus koji podseća na bakinu pitu sa višnjama.', '                  kore,višenje,ulje,šećer', 200, 'kg', 4, '9612'),
(5, 'Princes krofne', 'Iako su odavno odomacene na nasim stolovima, princes krofne su zapravo francuska poslastica.', 'brasno,ulje, jaja,secer,mleko,puter', 80, 'kom', 3, '8880'),
(6, 'Doboš', 'Doboš torta je sigurno jedna od najpoznatijih u svetu. Napravio je madjarski poslastičar sa ciljem da pronadje sastojke koji će moći da stoje i dve nedelje a da se ne pokvare. ', '                  bršno,sećer,ulje,čokolada ,prašak za pecivo,', 1500, 'kg', 2, '8679'),
(7, 'Plazma torta', 'Torta koju svi vole. Nikad se ne odbija', 'Mlevena plazma, mleko, puter, šećer u prahu,čokolada', 1000, 'kg', 2, '1468');

-- --------------------------------------------------------

--
-- Table structure for table `proizvod_korpa`
--

CREATE TABLE `proizvod_korpa` (
  `proizvod_korpa_id` int(10) UNSIGNED NOT NULL,
  `proizvod_id` int(10) UNSIGNED NOT NULL,
  `korpa_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `proizvod_korpa`
--

INSERT INTO `proizvod_korpa` (`proizvod_korpa_id`, `proizvod_id`, `korpa_id`) VALUES
(1, 2, 3),
(2, 2, 3),
(4, 2, 3),
(5, 2, 3),
(12, 4, 4),
(13, 2, 4),
(14, 2, 4),
(15, 2, 4),
(16, 6, 5),
(18, 4, 6),
(19, 6, 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`) USING BTREE;

--
-- Indexes for table `kategorija`
--
ALTER TABLE `kategorija`
  ADD PRIMARY KEY (`kategorija_id`) USING BTREE,
  ADD KEY `fk_kategorija_admin_id` (`admin_id`) USING BTREE;

--
-- Indexes for table `korpa`
--
ALTER TABLE `korpa`
  ADD PRIMARY KEY (`korpa_id`) USING BTREE;

--
-- Indexes for table `porudzbina`
--
ALTER TABLE `porudzbina`
  ADD PRIMARY KEY (`porudzbina_id`) USING BTREE,
  ADD KEY `fk_porudzbina_korpa_id` (`korpa_id`) USING BTREE;

--
-- Indexes for table `proizvod`
--
ALTER TABLE `proizvod`
  ADD PRIMARY KEY (`proizvod_id`) USING BTREE,
  ADD KEY `fk_proizvod_kategorija_id` (`kategorija_id`) USING BTREE;

--
-- Indexes for table `proizvod_korpa`
--
ALTER TABLE `proizvod_korpa`
  ADD PRIMARY KEY (`proizvod_korpa_id`) USING BTREE,
  ADD KEY `fk_proizvodKorpa_proizvod_id` (`proizvod_id`) USING BTREE,
  ADD KEY `fk_proizvodKorpa_korpa_id` (`korpa_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kategorija`
--
ALTER TABLE `kategorija`
  MODIFY `kategorija_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `korpa`
--
ALTER TABLE `korpa`
  MODIFY `korpa_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `porudzbina`
--
ALTER TABLE `porudzbina`
  MODIFY `porudzbina_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `proizvod`
--
ALTER TABLE `proizvod`
  MODIFY `proizvod_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `proizvod_korpa`
--
ALTER TABLE `proizvod_korpa`
  MODIFY `proizvod_korpa_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kategorija`
--
ALTER TABLE `kategorija`
  ADD CONSTRAINT `fk_kategorija_admin_id` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`admin_id`) ON UPDATE CASCADE;

--
-- Constraints for table `porudzbina`
--
ALTER TABLE `porudzbina`
  ADD CONSTRAINT `fk_porudzbina_korpa_id` FOREIGN KEY (`korpa_id`) REFERENCES `korpa` (`korpa_id`) ON UPDATE CASCADE;

--
-- Constraints for table `proizvod`
--
ALTER TABLE `proizvod`
  ADD CONSTRAINT `fk_proizvod_kategorija_id` FOREIGN KEY (`kategorija_id`) REFERENCES `kategorija` (`kategorija_id`) ON UPDATE CASCADE;

--
-- Constraints for table `proizvod_korpa`
--
ALTER TABLE `proizvod_korpa`
  ADD CONSTRAINT `fk_proizvodKorpa_korpa_id` FOREIGN KEY (`korpa_id`) REFERENCES `korpa` (`korpa_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_proizvodKorpa_proizvod_id` FOREIGN KEY (`proizvod_id`) REFERENCES `proizvod` (`proizvod_id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
