<?php
    use App\Core\Route;

    return [
        Route::get('#^admin/home/?$#',                             'AdminPocetna',                  'index'),
        Route::get('#^admin/logout/?$#',                           'Main',                          'loginOut'),
        Route::get('#^admin/login/?$#',                            'Main',                          'loginGet'),
        Route::post('#^admin/login/?$#',                           'Main',                          'loginPost'),
    
        Route::get('|^admin/proizvod/?$|',                         'AdminDodavanjeProizvoda',       'getAdd'),
        Route::get('|^admin/kategorija/?$|',                       'AdminDodavanjeKategorija',      'getAdd'),
        Route::post('|^admin/proizvod/?$|',                        'AdminDodavanjeProizvoda',       'postAdd'),
        Route::post('|^admin/kategorija/?$|',                      'AdminDodavanjeKategorija',      'postAdd'),
         
        Route::get('#^proizvodi/?$#',                              'Main',                          'home'),
        Route::get('#^kategorije/?$#',                             'Main',                          'showKategorijeProizvoda'),
        Route::get('|^proizvod/([0-9]+)/?$|',                      'Main',                          'showProizvod'),
        Route::post('|^proizvod/([0-9]+)/?$|',                     'ProizvodKorpa',                 'addProizvodKorpa'),
        Route::get('#^kategorija/([0-9]+)/?$#',                    'Kategorija',                    'show'),
        Route::post('|^search/?$|',                                'Proizvod',                      'postSearch'),
        Route::get('#^korpa/?$#',                                  'ProizvodKorpa',                 'getKorpa'),
        Route::post('#^korpa/?$#',                                 'ProizvodKorpa',                 'deleteProizvod'),
        Route::get('#^porudzbina/([0-9]+)/?$#',                    'Porudzbina',                    'getAdd'),
        Route::post('#^porudzbina/([0-9]+)/?$#',                   'Porudzbina',                    'postAdd'),
        
    
        Route::get('#^admin/proizvodi/?$#',                        'AdminDodavanjeProizvoda',       'proizvodi'),
        Route::get('#^admin/proizvodi/add/?$#',                    'AdminDodavanjeProizvoda',       'getAdd'),
        Route::post('#^admin/proizvodi/add/?$#',                   'AdminDodavanjeProizvoda',       'postAdd'),
        Route::get('#^admin/proizvodi/edit/([0-9]+)/?$#',          'AdminDodavanjeProizvoda',       'getEdit'),
        Route::post('#^admin/proizvodi/edit/([0-9]+)/?$#',         'AdminDodavanjeProizvoda',       'postEdit'),


        Route::get('#^admin/kategorije/?$#',                       'AdminDodavanjeKategorija',      'kategorije'),
        Route::get('#^admin/kategorije/add/?$#',                   'AdminDodavanjeKategorija',      'getAdd'),
        Route::post('#^admin/kategorije/add/?$#',                  'AdminDodavanjeKategorija',      'postAdd'),
        Route::get('#^admin/kategorije/edit/([0-9]+)/?$#',        'AdminDodavanjeKategorija',      'getEdit'),
        Route::post('#^admin/kategorije/edit/([0-9]+)/?$#',       'AdminDodavanjeKategorija',      'postEdit'),
       // Route::get('#^admin/kategorije/porudzbine/([0-9]+)/?$#',   'AdminDodavanjeKategorija',   'reservationsByProjection'),

       Route::get('#^dajsesiju/?$#',                       'Main',      'dajsesiju'),
         # Fallback
        Route::get('#^.*$#', 'Main', 'home')
    ];
