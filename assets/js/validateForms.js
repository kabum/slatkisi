function validateDodavanjeProizvodaForms() {
    let status = true;
    document.querySelector('#error-message').innerHTML = '';

    let naziv = document.getElementById('naziv').value;
    if (!naziv.match(/^([0-9a-zA-Z\.\'\! ]){3,}$/)) {
        document.querySelector('#error-message').innerHTML += 'Naziv mora sadrzati najmanje 3 vidljiva uzastopna karaktera.<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    let opis = document.getElementById('opis').value;
    if (!opis.match(/^([0-9a-zA-Z\.\'\! ]){3,}$/)) {
        document.querySelector('#error-message').innerHTML += 'Opis mora sadrzati najmanje 3 vidljiva uzastopna karaktera.<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    let sastojci = document.getElementById('sastojci').value;
    if (!sastojci.match(/^([0-9a-zA-Z\.\'\! ]){3,}$/)) {
        document.querySelector('#error-message').innerHTML += 'Mora sadrzati najmanje 3 vidljiva uzastopna karaktera.<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    
   

    let cena = document.getElementById('cena').value;
    if (!cena.match(/^[a-zA-Z ]{2,}$/)) {
        document.querySelector('#error-message').innerHTML += 'Cena mora sadrzati najmanje 3 vidljiva uzastopna karaktera.<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    let jed_mere = document.getElementById('jed_mere').value;
    if (!jed_mere.match(/^[0-9]{2,}$/)) {
        document.querySelector('#error-message').innerHTML += 'Jedinica mere mora imati minimum 2 uzastopna karaktera.<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    let kategorija = document.getElementById('kategorija').value;
    if (!kategorija.match(/^[a-zA-Z ]{2,}$/)) {
        document.querySelector('#error-message').innerHTML += 'Kategorija mora sadrzati najmanje 3 vidljiva uzastopna karaktera.<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    return status;

    
}
